from Tkinter import *
import sys
import os.path
import re
import operator
import ttk


class DataAnalyser():
    def __init__(self):
        pass

    def example_function(self, dataset1, dataset2):
        """This is a function example

        Parameters:
        dataset1 (list):    A list of dictionaries of "government pocurement via gebiz"
                            The keys in the dictionaries are the same as the column headers in
                            the "government pocurement via gebiz"csv file

                            Eg.
                            dataset1[0]["agency"]
                            This will give you the agency of the first line of data

                            Eg.
                            dataset1[35]["supplier_name"]
                            This will give you the supplier name of line 36 of data

        dataset2 (list):    A list of dictionaries of "listing of registered contractors"
                            The keys in the dictionaries are the same as the column headers in
                            the "listing of registereed contractors" csv file
        """

        # This prints out all the award dates in "government pocurement via gebiz"
        for item in dataset1:
            print item["award_date"]

        # This prints out all the company names in "listing of registereed contractors"
        for item in dataset2:
            print item["company_name"]

        return 0

    def create_folder_at_directory(self, folder_name, directory):
        """Creates a folder at the specified directory

        Parameters:
        folder_name (string):   The name of the folder to create
                                If the folder already exists, a number will be added
                                to the back of the folder name

        directory (list):       The directory to create the folder in
        """

        new_folder_name = "/" + folder_name
        num = 1
        while os.path.exists(directory + new_folder_name):
            new_folder_name = "/Agency Procurement_" + str(num)
            num += 1
        os.mkdir(directory + new_folder_name)
        return directory + new_folder_name

    def export_agency_procurement(self, dataset1, export_path):
        """Exports each agency's procurement into their own text file(Function 2)

        Parameters:
        dataset1 (list):        The government agency procurement data set

        export_path (string):   The directory to export the text files to
        """

        agencies = {}
        for data in dataset1:
            if data["agency"] not in agencies.keys():
                agencies[data["agency"]] = []
            agencies[data["agency"]].append(data)

        folder_path = self.create_folder_at_directory("Agency Procurement", export_path)

        for key, value in agencies.items():
            file_name = "/" + re.sub('[^A-Za-z0-9]+', '', key) + ".txt"
            f = open(folder_path + file_name, "w+")
            f.write("Procurement for the agency of: " + key + "\n")
            total_amt = 0
            for item in value:
                try:
                    total_amt += int(item["awarded_amt"])
                except ValueError:
                    pass
            f.write("Total awarded amount: " + str(total_amt) + "\n\n")
            for item in value:
                f.write("Tender no.\t: " + item["tender_no."] + "\n")
                f.write("Award date\t: " + item["award_date"] + "\n")
                f.write("Awarded amt\t: " + item["awarded_amt"] + "\n")
                f.write("Supplier\t\t: " + item["supplier_name"] + "\n")
                f.write("Detail status\t: " + item["tender_detail_status"] + "\n")
                f.write("Description\t: " + item["tender_description"] + "\n\n")
            f.close()

    # Try to write your functions in the specified spot to reduce merge conflicts
    # Also please rename your function to something more descriptive
    def list_procurements(self, dataset1):
        """Display number of procurements from each government sector in either increasing/decreasing order(Function 3)

        Parameters:
        dataset1 (list):    The government agency procurement data set
        """

        agencies = {}
        total_procurements = {}

        for data in dataset1:
            if data["agency"] not in agencies.keys():
                agencies[data["agency"]] = []
            agencies[data["agency"]].append(data)

        for key, value in agencies.items():
            for item in value:
                if item["agency"] not in total_procurements:
                    total_procurements[item["agency"]] = 1
                else:
                    total_procurements[item["agency"]] += 1

        sorted_dictionary = sorted(total_procurements.items(), key=operator.itemgetter(1))
        print "Total procurements for each government sectors in increasing order: ", sorted_dictionary

        sorted_dictionary = sorted(total_procurements.items(), key=operator.itemgetter(1), reverse=True)
        print "Total procurements for each government sectors in decreasing order: ", sorted_dictionary

    def list_awarded_registered(self, dataset1, dataset2):
        """List down the awarded vendors which are the registered contractors. (Function 4)

         Parameters:
         dataset1 (list):    The government agency procurement data set
         dataset2 (list):    The government agency procurement data set
         """

        registered_vendors = []

        total_dict = {}
        for data in dataset1:
            dict1 = {data['supplier_name']: data}
            total_dict.update(dict1)

        registered_dict = {}

        for data in dataset2:
            dict1 = {data['company_name']: data}
            registered_dict.update(dict1)

        for name in total_dict.keys():
            if name in registered_dict.keys():
                registered_vendors.append(total_dict[name])

        awarded_dict = {}
        for data in registered_vendors:
            if float(data["awarded_amt"]) > 0:
                awarded_dict[data["supplier_name"]] = data["awarded_amt"]
        awarded_dict = sorted(awarded_dict.keys())
        print len(total_dict)
        print awarded_dict

    def awarded_sort_key(self, i):  # sort key for list_awarded_contractors()
        return float(i['awarded_amt'])

    def list_awarded_contractors(self, dataset1, dataset2):  # function 5
        """List down the awarded vendors which are the registered contractors. (Function 5)

        Parameters:
        dataset1 (list):    The government agency procurement data set
        dataset2 (list):    The government agency procurement data set
        """

        registered_list = []
        non_registered_list = []

        hybriddict = {}
        for i in dataset1:  # set name as key and data as value
            dict = {i['supplier_name'].upper(): i}
            hybriddict.update(dict)

        regdict = {}
        for i in dataset2:  # set name as key and data as value
            dict = {i['company_name'].upper(): i}
            regdict.update(dict)

        for name in hybriddict.keys():
            if name in regdict.keys():  # compare and sort/filter non/registered
                registered_list.append(hybriddict[name])  # found
            else:
                non_registered_list.append(hybriddict[name])  # not found

        registered_list = sorted(registered_list, key=self.awarded_sort_key, reverse=True)
        non_registered_list = sorted(non_registered_list, key=self.awarded_sort_key, reverse=True)

        count_reg = count_non = 0
        print ("Top 5 Registered Contractors")
        for i in registered_list:
            if count_reg < 5:
                count_reg += 1
                print "%5d.)%15s($)\t|%s" % (count_reg, i['awarded_amt'], str(i['supplier_name']))
        print ("\nTop 5 Non-Registered Contractors")
        for i in non_registered_list:
            if count_non < 5:
                count_non += 1
                print ("%5d.)%15s($)\t|%s" % (count_non, i['awarded_amt'], str(i['supplier_name'])))

    def list_monthly_procurement(self, dataset1, frame_parent):
        """Displays the procurement for every month, and the prediction for the next X months

        Parameters:
        dataset1 (list):        The government agency procurement data set
        frame_parent (Frame):   The frame parent for the monthly procurement frame
        """

        frame = Frame(frame_parent)
        Label(frame, text = "Monthly procurement").grid(row = 0, column = 0, sticky = "w")
        monthly_listbox = Listbox(frame)
        monthly_listbox.grid(row = 1, column = 0, sticky = "w")

        #Add items to listbox
        for item in ["example1", "example2", "example3", "Month 1 : 12345", "Month 2 : 5324543"]:
            monthly_listbox.insert(END, item)

        return frame

    def function6(self, dataset1, dataset2):
        print "function 6"
