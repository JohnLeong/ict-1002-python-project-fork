from Tkinter import *
from data_analyser import *
from data_table import *
import tkFileDialog
import ttk
import os.path
import csv

class Application(Frame):
    CONST_APP_NAME = "1002 Python Project"
    CONST_WINDOW_MIN_WIDTH = 700
    CONST_WINDOW_MIN_HEIGHT = 300
    CONST_PATH_ICON = "icon/sit_icon.ico"

    def __init__(self, master=None):
        self.selectedFilePath_01 = "No file selected"
        self.selectedFilePath_02 = "No file selected"
        self.selected_export_path = "No path specified"
        self.dataset1 = []
        self.dataset2 = []
        self.data_analyser = DataAnalyser()
        self.process_data_finished = False
        self.current_frame_displayed = None
        self.all_display = None
        self.monthly_display = None

        Frame.__init__(self, master)
        master.title(Application.CONST_APP_NAME)
        master.minsize(Application.CONST_WINDOW_MIN_WIDTH, Application.CONST_WINDOW_MIN_HEIGHT)
        master.iconbitmap(Application.CONST_PATH_ICON)
        #master.resizable(False, False)
        self.pack(anchor = "w", padx = (10, 10), pady = (10, 10))
        self.create_widgets()

    def create_widgets(self):
        """Creates the program window using tkinter"""

        #Tab controls
        self.tab_control = ttk.Notebook(self)
        self.tab_control.grid(row = 0, column = 0, sticky = "w")

        "----------------------------------------------------------------------------------------------------------"
        #File selection frame
        self.local_data_frame = Frame(self.tab_control)
        self.local_data_frame.grid(row = 0, column = 0, sticky = "w")
        self.local_data_frame.grid_rowconfigure(0, minsize=10)
        self.tab_control.add(self.local_data_frame, text = "Read data from file")

        self.file_select_frame = LabelFrame(self.local_data_frame, text = "File selection", width = 800)
        self.file_select_frame.grid(row = 1, column = 0, sticky = "w")
        self.file_select_frame.grid_columnconfigure(3, minsize = 450)
        self.file_select_frame.grid_rowconfigure(0, minsize=10)

        self.file_button_01 = Button(self.file_select_frame, text = "Select a file", command = self.select_file_01)
        self.file_button_01.grid(row = 1, column = 0, sticky = "w", padx = 10)
        self.file_label_01 = Label(self.file_select_frame, text = "Procurement Dataset: ")
        self.file_label_01.grid(row = 1, column = 1, sticky = "w")
        self.selected_file_label_01 = Label(self.file_select_frame, text = self.selectedFilePath_01, anchor = "w")
        self.selected_file_label_01.grid(row = 1, column = 3, sticky = "w")

        self.file_button_02 = Button(self.file_select_frame, text = "Select a file", command = self.select_file_02)
        self.file_button_02.grid(row = 2, column = 0, sticky = "w", padx = 10)
        self.file_label_02 = Label(self.file_select_frame, text = "Contractors Dataset: ")
        self.file_label_02.grid(row = 2, column = 1, sticky = "w")
        self.selected_file_label_02 = Label(self.file_select_frame, text = self.selectedFilePath_02, anchor = "w")
        self.selected_file_label_02.grid(row = 2, column = 3, sticky = "w")

        self.export_path_button = Button(self.file_select_frame, text = "Select path", command = self.select_export_path)
        self.export_path_button.grid(row = 3, column = 0, sticky = "w", padx = 10)
        self.export_path_label = Label(self.file_select_frame, text = "Export path: ")
        self.export_path_label.grid(row = 3, column = 1, sticky = "w")
        self.export_path_label = Label(self.file_select_frame, text = self.selected_export_path, anchor = "w")
        self.export_path_label.grid(row = 3, column = 3, sticky = "w")

        self.file_select_frame.grid_rowconfigure(4, minsize=10)
        self.process_button = Button(self.file_select_frame, text = "Process Data", command = self.process_file_data)
        self.process_button.grid(row = 5, column = 0, sticky = "w", padx = 10, pady = 10)

        #Dataset functions frame
        self.local_data_frame.grid_rowconfigure(2, minsize = 20)
        self.func_data_frame = Frame(self.local_data_frame)
        self.func_data_frame.grid(row = 3, column = 0, sticky = "w")

        self.functions_frame = LabelFrame(self.func_data_frame, text = "Dataset functions")
        self.functions_frame.grid(row = 0, column = 0, sticky = "wne")
        Button(self.functions_frame, text = "View all procurement", command = self.display_procurement).grid(row = 0, column = 0, sticky = "w")
        Button(self.functions_frame, text = "View monthly procurement", command = self.monthly_procurement).grid(row = 1, column = 0, sticky = "w")
        Button(self.functions_frame, text = "FUNCTION 3", command=self.list_procurements).grid(row=2, column=0, sticky="w")
        Button(self.functions_frame, text = "FUNCTION 4", command = self.list_awarded_registered).grid(row = 3, column = 0, sticky = "w")
        Button(self.functions_frame, text = "FUNCTION 5", command=self.list_awarded_contractors).grid(row=4, column=0, sticky="w")

        #Export functions frame
        self.export_functions_frame = LabelFrame(self.func_data_frame, text = "Export functions")
        self.export_functions_frame.grid(row = 1, column = 0, sticky = "wne")
        self.export_procurement_button = Button(self.export_functions_frame, text = "Export procurement", command = self.export_agency_procurement)
        self.export_procurement_button.grid(row = 0, column = 0, sticky = "w")

        #Display frame
        self.display_frame = LabelFrame(self.func_data_frame, text = "Data display", width = 450, height = 200)
        self.display_frame.grid(row = 0, column = 1, rowspan = 2, sticky = "wn")

        "----------------------------------------------------------------------------------------------------------"
        #Real-time data reading frame
        self.live_data_frame = Frame(self.tab_control)
        self.live_data_frame.grid(row = 0, column = 0, sticky = "w")
        self.live_data_frame.grid_rowconfigure(0, minsize=10)
        self.tab_control.add(self.live_data_frame, text = "Read data real-time")

        self.read_realtime_frame = LabelFrame(self.live_data_frame, text = "Live data reading", width = 400)
        self.read_realtime_frame.grid(row = 1, column = 0, sticky = "w")

        Button(self.read_realtime_frame, text = "Read live data").grid(row = 0, column = 0, sticky = "w")

    def select_file_01(self):
        """Displays a popup for the user to select the pocurement dataset file from their computer"""

        self.selectedFilePath_01 = tkFileDialog.askopenfilename(initialdir = "/", title = "Select file", filetypes = (("csv files","*.csv"),("all files","*.*")))
        self.selectedFilePath_01 = "No file selected" if len(self.selectedFilePath_01) < 1 else self.selectedFilePath_01
        self.selected_file_label_01["text"] = self.selectedFilePath_01

    def select_file_02(self):
        """Displays a popup for the user to select the contractors dataset file from their computer"""

        self.selectedFilePath_02 = tkFileDialog.askopenfilename(initialdir = "/", title = "Select file", filetypes = (("csv files","*.csv"),("all files","*.*")))
        self.selectedFilePath_02 = "No file selected" if len(self.selectedFilePath_02) < 1 else self.selectedFilePath_02
        self.selected_file_label_02["text"] = self.selectedFilePath_02

    def select_export_path(self):
        """Displays a popup for the user to select the export path"""

        self.selected_export_path = tkFileDialog.askdirectory()
        self.selected_export_path = "No path specified" if len(self.selected_export_path) < 1 else self.selected_export_path
        self.export_path_label["text"] = self.selected_export_path

    def process_file_data(self):
        """Processes the data in the 2 provided datasets and stores them in 2 lists"""

        #Checks if the given file paths are valid
        if not(os.path.exists(self.selectedFilePath_01)) or not(os.path.exists(self.selectedFilePath_02)):
            print("Error: Invalid file path")
            return False

        self.read_filedata_to_list(self.selectedFilePath_01, self.dataset1)
        self.read_filedata_to_list(self.selectedFilePath_02, self.dataset2)

        #If a data_tree Frame already exists, remove it from the grid and delete it
        if self.all_display:
            self.all_display.grid_forget()
            del self.all_display
        #Create a new data table based on dataset 1
        self.all_display = DataTable(self.display_frame, self.dataset1, 10)

        self.process_data_finished = True
        return True

    def read_filedata_to_list(self, filepath, target_list):
        """Reads the file at the specified file path and stores the data in target_list"""

        print "Reading file at " + filepath

        #Check for a valid file path
        if not(os.path.exists(filepath)):
            return False

        #Reads file line by line and stores the data in target_list
        #Values in the datasets are seperated by commas
        with open(filepath) as csvfile:
            read_csv = csv.reader(csvfile, delimiter=',')

            #Read the first line to form the column categories
            categories = next(read_csv)
            for line in read_csv:
                container = {}
                for i in range(len(categories)):
                    container[categories[i]] = line[i]
                target_list.append(container)

        print "Finished reading file"
        return True

    def set_display_frame(self, new_frame):
        """Sets the current frame in the 'Data display frame' to new_frame"""
        if self.current_frame_displayed:
            self.current_frame_displayed.grid_forget()
        self.current_frame_displayed = new_frame
        self.current_frame_displayed.grid(row = 0, column = 0)

    def display_procurement(self):
        """Displays every single data in dataset1 using the DataTable class"""
        if not self.process_data_finished:
            print("Data not found. Please input datasets.")
            return

        self.set_display_frame(self.all_display)

    def monthly_procurement(self):
        """Displays the procurement for every month, and the prediction for the next X months"""
        if not self.process_data_finished:
            print("Data not found. Please input datasets.")
            return
        if self.monthly_display:
            self.monthly_display.grid_forget()
            del self.monthly_display
        self.monthly_display = self.data_analyser.list_monthly_procurement(self.dataset1, self.display_frame)

        self.set_display_frame(self.monthly_display)

    def export_agency_procurement(self):
        """Exports each agency's procurement into their own text file(Function 2)"""
        if not self.process_data_finished or not os.path.exists(self.selected_export_path):
            print("Invalid export path")
            return
        self.data_analyser.export_agency_procurement(self.dataset1, self.selected_export_path)

    def list_procurements(self):
        """Exports each agency's procurement into their own text file (Function 3)"""
        if not self.process_data_finished or not os.path.exists(self.selected_export_path):
            print("Invalid export path")
            return
        self.data_analyser.list_procurements(self.dataset1)

    def list_awarded_registered(self):
        """List the awarded vendors which are the registered contract (Function 4)"""
        if not self.process_data_finished:
            print("Data not found. Please input datasets.")
            return
        self.data_analyser.list_awarded_registered(self.dataset1, self.dataset2)

    def list_awarded_contractors(self):
        """Temporarily prints (graph/table) total procurement awarded to non/registered contractors + top 5 awarded (Function 5)"""
        if not self.process_data_finished:
            print("Data not found. Please input datasets.")
            return
        self.data_analyser.list_awarded_contractors(self.dataset1, self.dataset2)
