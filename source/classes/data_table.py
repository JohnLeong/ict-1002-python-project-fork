from Tkinter import *
import ttk
import math

class DataTable(Frame):
    def __init__(self, parent, dataset, max_rows = 10):
        Frame.__init__(self, parent)
        self.current_page = 0
        self.dataset = dataset
        self.max_rows = max_rows

        #Create the table using tkinter's treeview
        self.treeview = ttk.Treeview(self)
        self.treeview.grid(row = 0, column = 0, sticky = "w")
        self.treeview['show'] = 'headings'

        if len(dataset) > 0:
            self.treeview["columns"] = dataset[0].keys()
            for key in dataset[0].keys():
                self.treeview.heading(key, text = key)
                self.treeview.column(key, width = 75, stretch = False)

            for i in range(self.max_rows):
                self.treeview.insert("", i, i, values = ())

            self.update_treeview()

        #Create scrollbars
        self.horizontal_scrollbar = ttk.Scrollbar(self, orient = HORIZONTAL)
        self.horizontal_scrollbar.configure(command = self.treeview.xview)
        self.horizontal_scrollbar.grid(row = 1, column = 0, sticky = "we")
        self.treeview.configure(xscrollcommand = self.horizontal_scrollbar.set)

        self.vertical_scrollbar = ttk.Scrollbar(self, orient = VERTICAL)
        self.vertical_scrollbar.configure(command = self.treeview.yview)
        self.vertical_scrollbar.grid(row = 0, column = 1, sticky = "ns")
        self.treeview.configure(yscrollcommand = self.vertical_scrollbar.set)

        #Create page buttons
        self.page_selection_frame = Frame(self)
        self.page_selection_frame.grid(row = 2, column = 0, sticky = "w")
        self.left_button = Button(self.page_selection_frame, text = "Prev page", command = self.prev_page)
        self.left_button.grid(row = 0, column = 0, sticky = "w")
        self.right_button = Button(self.page_selection_frame, text = "Next page", command = self.next_page)
        self.right_button.grid(row = 0, column = 1, sticky = "w")
        self.page_label = Label(self.page_selection_frame, text = "Page 1")
        self.page_label.grid(row = 0, column = 2, sticky = "w")

    def prev_page(self):
        """Displays the previous page in the table"""
        if self.current_page < 1:
            return
        self.current_page -= 1
        self.page_label["text"] = "Page " + str(self.current_page + 1)
        self.update_treeview()

    def next_page(self):
        """Displays the next page in the table"""
        if self.current_page + 2 > int(math.ceil(len(self.dataset) / self.max_rows)):
            return
        self.current_page += 1
        self.page_label["text"] = "Page " + str(self.current_page + 1)
        self.update_treeview()

    def update_treeview(self):
        """Updates the table to display the results from the current page"""
        x = self.treeview.get_children()
        for i in range(len(x)):
            if self.current_page * self.max_rows + i < len(self.dataset):
                self.treeview.item(x[i], values = self.dataset[self.current_page * self.max_rows + i].values())
            else:
                self.treeview.item(x[i], values = ())
